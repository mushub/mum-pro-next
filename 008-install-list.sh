#!/usr/bin/env bash

############################################################
# Makes the script more portable ###########################
readonly DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
############################################################

readonly inlist="${DIR}/install-list.txt"

yay  -S --needed  - < $inlist

sudo pacman -S - < $inlist		    

figlet -c -f smslant *INSTALLED*

figlet -c -f smslant ${user}

