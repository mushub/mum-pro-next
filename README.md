### MUM-Pro-next. 
<pre><code>PORTABLE VERSION :: Tested on Arcolinux and Garuda :: use on your own risk >>></code></pre>


> ![ArcoLinux_2020-11-28_16-43-52](/uploads/7ce6c8298ade39f61d81b2fc22a18280/ArcoLinux_2020-11-28_16-43-52.png)

### How to run MUM ::

> ###### `$ git clone https://gitlab.com/mushub/mum-pro-next.git`

### Run MUM from mum-pro-next folder, anyware on your system.
> ###### `$ ./update-manager`

#### - --- Before doing anything with MUM :: DO :: cd to mum-pro-next folder and make scripts executable.


> ###### `$ chmod +x 001-RM-remove-list.sh 002-RM-install-list.sh 004-add-install-list.sh 005-add-remove-list.sh 007-remove-list.sh 008-install-list.sh remove.sh search.sh update-manager`


### This is needed to give MUM the basic to run. 
> ###### `$ sudo pacman -S yay terminator konsole figlet `


- ###### _Terminator is for MUM_
- ###### _Konsole and yay :: needed for installing dependencies  ::_
- ###### _Figlet for fancy comments_
- ###### _Find a good font for emoji support :: source code pro bold ::_


------------------------------------------------------------------------------------------

#### Terminator works nicely. Easy to setup.
###### `$ bash ~/mum-pro-next/update-manager`. 
> ![Schermafdruk_2020-11-23_16-45-36](/uploads/0fcd4df7c2c043aab718cd5c782cbadf/Schermafdruk_2020-11-23_16-45-36.png)



## **RUN MUM**
> ##### - --- Go to install packlist button in the MUM menu and run the script to install all other dependencies. logout/in.
> ![Schermafdruk_2020-11-23_16-51-39](/uploads/e239360b02f4747bbc04f3e257ff5941/Schermafdruk_2020-11-23_16-51-39.png)
> ##### - --- MUM needs to refresh to see changes.


 ⚠ ::_JUDGE AND CHECK :: INSTALL-LIST & REMOVE-LIST_ :: ⚠


#### Source script 

> https://askubuntu.com/questions/1705/how-can-i-create-a-select-menu-in-a-shell-script
-------------------------------------------------------------------------------------------------------------------------

#### Video demo MUM and arcolinux ::  {previous version}
> https://d.tube/v/linux-bird-020/QmXfPD47Kn5Rsz2t5Jxk9XXkyM6dHACmhQHJNQku4WraHU

###### D3m0 edit install-pack-list and remove-pack-list
> ![Peek_2020-11-28_19-59](/uploads/90427d1efcac98eb1a59c5d32d5b56ae/Peek_2020-11-28_19-59.gif)
> ![ArcoLinu_wwww](/uploads/9b25eb839ad6cef94ff068e17ceeca87/ArcoLinu_wwww.png)

######  Todo : 
- edit 001 and 002 script - figlet - for nice comments 
- edit Readme / cleanup
